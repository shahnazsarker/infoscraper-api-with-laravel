<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DomainContoller extends Controller
{
  public function index(Request $request) {

      $domainName = ['domainName' => $request->input('domainName')];
      //print_r($request->all());
      $url = 'https://' . $domainName['domainName'];
      if (isset($url)) {
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_HEADER, 1);

          $response = curl_exec($ch);

          // Return headers separately from the Response Body

          $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
          $headers = substr($response, 0, $header_size);
         //echo $headers;
          $headersArray = explode("\r\n", $headers);
          curl_close($ch);
          //print_r($headersArray);
          $result = ["Domain Name"=>$domainName["domainName"], "Server"=>"Unavailable", "SSL"=>"No", "CMS"=>"No"];
         foreach ($headersArray as $value) {
             $tempArray = explode(": ", $value);
            if (in_array("server", $tempArray)) {
                 $result["Server"] = $tempArray[1];

            }

             if (in_array("alt-svc", $tempArray)) {
                 $result["SSL"] =  "Yes" ;

             }

             if (in_array("x-generator", $tempArray)) {
//                 $tempArray[0]= 'CMS';
                $result['CMS'] = $tempArray[1];
             }

         }

      }
      return json_encode($result);

  }
}
