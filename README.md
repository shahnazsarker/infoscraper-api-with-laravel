# Info Scraper API with Laravel
An API that scrapes informaion of given domain name. It provides domain's Server name, whether SSL key available or not and CMS information.
## Flowchart of the Info Scraper API
![infoscraper-with-laravel-flowchart](/uploads/0d68d997bf7b51a26ca62fdbc2dc00e9/infoscraper-with-laravel-flowchart.png)
